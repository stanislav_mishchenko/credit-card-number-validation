﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using CreditCardNumberValidator.Models;

namespace CreditCardNumberValidator.Tools
{
    /// <summary>
    /// Card validator.
    /// </summary>
    public static class CardValidator
    {

        /// <summary>
        /// Digitis only and length regex pattern
        /// </summary>
        private const string _digitsOnlyAndLengthPattern = @"^\d{15,16}$";

        /// <summary>
        /// Prime numbers
        /// </summary>
        private static readonly Lazy<HashSet<byte>> _primeNumbers = new Lazy<HashSet<byte>>(() =>
            {
                // prime numbers less than 100
                byte[] arr = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
                return new HashSet<byte>(arr);
            });

        /// <summary>
        /// The calendar.
        /// </summary>
        private static readonly Lazy<GregorianCalendar> _calendar = new Lazy<GregorianCalendar>();


        /// <summary>
        /// Validate the specified card number and expiration date.
        /// </summary>
        /// <returns>boolean - valid or invalid, card type</returns>
        /// <param name="cardNumber">Card number.</param>
        /// <param name="expiry">Expiration date.</param>
        public static Tuple<bool, CardTypes> Validate(string cardNumber, DateTime expiry){

            var len = cardNumber?.Length;

            // if card number length not in range, or contains not only digits
            if ( !Regex.Match(cardNumber, _digitsOnlyAndLengthPattern).Success){
                return new Tuple<bool, CardTypes>(false, CardTypes.Unknown);
            }

            // Only Amex card number length is 15 digits and strarts with 3
            if (len == 15)
            {
                if (cardNumber[0] == '3')
                {
                    return new Tuple<bool, CardTypes>(true, CardTypes.Amex);
                }
                else
                {
                    return new Tuple<bool, CardTypes>(false, CardTypes.Unknown);
                }
            }

            // check first number
            switch (cardNumber[0])
            {
                // JCB , all jcb cards are valid
                case '3':
                    return new Tuple<bool, CardTypes>(true, CardTypes.JCB);

                // Visa
                case '4': 
                    // the year is leap or not 
                    bool isLeapYear = _calendar.Value.IsLeapYear(expiry.Year);
                    return new Tuple<bool, CardTypes>(isLeapYear, CardTypes.Visa);

                // MasterCard
                case '5':
                    // get year 
                    // 1914, 2014 and 3014 %100 would result in 14.
                    int year = expiry.Year % 100;

                    // check prime number
                    bool isPrime = _primeNumbers.Value.Contains((byte)year);

                    return new Tuple<bool, CardTypes>(isPrime, CardTypes.MasterCard);

                // Unknown
                default:
                    return new Tuple<bool, CardTypes>(false, CardTypes.Unknown);
            }
        }
    }
}
