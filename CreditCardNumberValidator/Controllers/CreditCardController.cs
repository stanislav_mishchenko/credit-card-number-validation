﻿using System;
using System.Threading.Tasks;
using CreditCardNumberValidator.Models;
using CreditCardNumberValidator.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CreditCardNumberValidator.Controllers
{
    /// <summary>
    /// Credit Card controller.
    /// </summary>   
    [Route("api/[controller]")]
    [ApiController]
	public class CardController : ControllerBase
	{
		private readonly CardContext _context;
        protected readonly ILogger<CardController> _logger;

        public CardController(CardContext context, ILogger<CardController> logger)
		{
			_context = context;
            _logger = logger;
		}

		// Post api/card
		/// <summary>
		/// Validate credit card number
		/// </summary>
		//[HttpPost]
        [HttpGet("{cardNumber}/{expiry}")]
        public async Task<ActionResult<ValidationResponse>> Validate(string cardNumber, DateTime expiry)         
        {
            var response = new ValidationResponse()
            {
                CardType = "Unknown",
                Result = "Invalid",
            };

            // validate card
            var validationResult = CardValidator.Validate(cardNumber?.Trim(), expiry);

            // set card type, result still invalid
            response.CardType = validationResult.Item2.ToString();

            // if card is not valid
            if (!validationResult.Item1)
            {
                _logger.LogInformation("Validation result for card '{0}' '{1}': {2} {3}",
                                       cardNumber, expiry, response.Result, response.CardType);
                return response;
            }

            // is card exists
            bool is_exist = await _context.IsCardExist(cardNumber, expiry);

            // compose 
            response.Result = is_exist ? "Valid" : "Does not exist";

            _logger.LogInformation("Validation result for card '{0}' '{1}': {2} {3}", 
                                   cardNumber, expiry, response.Result, response.CardType);

            return response;
        }
    }
}
