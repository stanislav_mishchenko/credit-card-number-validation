﻿use ccn_test;
GO

CREATE PROCEDURE IsCardExist 
@CardNumber NCHAR(16), 
@Expiry DATE -- expiration date 
AS  
    IF EXISTS( 
            SELECT * FROM dbo.CreditCards 
            WHERE CardNumber = @CardNumber 
                AND MONTH(Expiry) = MONTH(@Expiry)
                AND YEAR(Expiry) = YEAR(@Expiry) )
        RETURN 1 -- card found
    ELSE        
        RETURN 0;
GO