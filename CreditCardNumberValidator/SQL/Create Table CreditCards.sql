﻿use ccn_test;
GO
CREATE TABLE dbo.CreditCards(
    Id BIGINT IDENTITY(1, 1) PRIMARY KEY NOT NULL,
    CardNumber NCHAR(16) NOT NULL,
    Expiry DATE NOT NULL,
    CONSTRAINT UI_CardNumber UNIQUE(CardNumber)) 
GO