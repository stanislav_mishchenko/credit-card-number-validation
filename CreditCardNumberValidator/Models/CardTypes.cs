﻿using System;
namespace CreditCardNumberValidator.Models
{
	/// <summary>
    /// Credit Card types.
    /// </summary>
    public enum CardTypes
    {
		Unknown = 0,
        Visa,
        MasterCard,
        Amex,
        JCB,
    }
}
