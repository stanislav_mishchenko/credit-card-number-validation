﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CreditCardNumberValidator.Models
{
    /// <summary>
    /// Card data context.
    /// </summary>
	public class CardContext: DbContext
    {
		public CardContext(DbContextOptions<CardContext> options): base(options)            
        {
        }

        /// <summary>
        /// Is card exist. Calls stored procedure
        /// </summary>
        /// <returns>The card exist or not</returns>
        /// <param name="cardNumber">Card number.</param>
        /// <param name="expiry">Expiration date.</param>
        public virtual async Task<bool> IsCardExist( string cardNumber, DateTime expiry){

            SqlParameter[] @params =
            {
                new SqlParameter("@CardNumber", cardNumber),
                new SqlParameter("@Expiry", expiry),
                new SqlParameter("@Exist", SqlDbType.Bit) {Direction = ParameterDirection.Output}
            };

            await this.Database.ExecuteSqlCommandAsync(
                "EXECUTE @Exist = isCardExist @CardNumber, @Expiry", @params);

            bool result = (bool)@params[2].Value;
            return result;
        }

    }
}
