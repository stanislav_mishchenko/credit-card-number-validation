﻿using System;
namespace CreditCardNumberValidator.Models
{
    public class ValidationResponse
    {

        /// <summary>
        /// Gets or sets the validation result.
        /// </summary>
        /// <value>The result.</value>
		public string Result { get; set; }

        /// <summary>
        /// Gets or sets the card types.
        /// </summary>
        /// <value>The card types.</value>
		public string CardType { get; set; }
    }
}
