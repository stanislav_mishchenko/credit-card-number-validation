using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Moq;
using CreditCardNumberValidator.Controllers;
using CreditCardNumberValidator.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CCNVTests
{
    public class CardNumberValidationTest
    {
        /// <summary>
        /// Test data.
        /// </summary>
        private List<Tuple<string, DateTime>> _testData;

        /// <summary>
        /// Test context.
        /// </summary>
        private readonly Mock<CardContext> _mockContext;

        public CardNumberValidationTest(){
            // init test data
            _testData = new List<Tuple<string, DateTime>>{
                // valid visa
                new Tuple<string, DateTime>("4004567890123456", new DateTime(2020, 04, 01)),
                // valid mastercard 
                new Tuple<string, DateTime>("5004567890123456", new DateTime(2019, 04, 01)),
                // valid amex
                new Tuple<string, DateTime>("300456789012345", new DateTime(2021, 04, 01)),
                // valid jcb
                new Tuple<string, DateTime>("3004567890123456", new DateTime(2018, 12, 01)),
            };

            // init database options 
            var dbOptions = new DbContextOptionsBuilder<CardContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            // init context
            _mockContext = new Mock<CardContext>(dbOptions);
            // init "stored procedure"
            _mockContext.Setup(c => c.IsCardExist(It.IsAny<string>(), It.IsAny<DateTime>()))
                        .ReturnsAsync((string arg1, DateTime arg2) => MockIsCardExist(arg1, arg2));

        }

        /// <summary>
        /// Mocks "is card exist" stored procedure
        /// </summary>
        /// <returns><c>true</c>, if is card exist <c>false</c> otherwise.</returns>
        /// <param name="cardNumber">Card number.</param>
        /// <param name="expiry">Expiration date.</param>
        private bool MockIsCardExist(string cardNumber, DateTime expiry){
                
            var result = _testData.Any(o =>
                   o.Item1 == cardNumber // compare card number
                   && o.Item2.Year == expiry.Year // compare expiry year
                   && o.Item2.Month == expiry.Month); // compare expiry month

            return result;
        }

        /// <summary>
        /// Valid visa card test.
        /// </summary>
        [Fact]
        public async Task ValidVisaCard(){
            // visa card number starts from 4 and has length 16
            string cardNumber = "4004567890123456";
            // year must be a leap year
            DateTime expiryDate = new DateTime(2020, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Valid", result.Value.Result);
            Assert.Equal("Visa", result.Value.CardType);
        }

        /// <summary>
        /// Valid mastercard test.
        /// </summary>
        [Fact]
        public async Task ValidMasterCard(){

            // mastercard number starts from 5 and has length 16
            string cardNumber = "5004567890123456";
            // year must be a prime number
            DateTime expiryDate = new DateTime(2019, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Valid", result.Value.Result);
            Assert.Equal("MasterCard", result.Value.CardType);
        }

        /// <summary>
        /// Valid amex card test.
        /// </summary>
        [Fact]
        public async Task ValidAmexCard(){
            // amex card number starts from 3 and has length 15
            string cardNumber = "300456789012345";
            // expiration date
            DateTime expiryDate = new DateTime(2021, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Valid", result.Value.Result);
            Assert.Equal("Amex", result.Value.CardType);
        }

        /// <summary>
        /// Valid JCB card test.
        /// </summary>
        [Fact]
        public async Task ValidJCBCard()
        {
            // jcb card number starts from 3 and has length 16
            string cardNumber = "3004567890123456";
            // expiration date
            DateTime expiryDate = new DateTime(2018, 12, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Valid", result.Value.Result);
            Assert.Equal("JCB", result.Value.CardType);
        }

        /// <summary>
        /// Invalid visa card test.
        /// </summary>
        [Fact]
        public async Task InvalidVisaCard(){
            // visa card number starts from 4 and has length 16
            string cardNumber = "4004567890123456";
            // Not a leap year
            DateTime expiryDate = new DateTime(2021, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var response = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Invalid", response.Value.Result);
            Assert.Equal("Visa", response.Value.CardType);
        }

        /// <summary>
        /// Invalid master card test.
        /// </summary>
        [Fact]
        public async Task InvalidMasterCard(){
            // mastercard number starts from 5 and has length 16
            string cardNumber = "5004567890123456";
            // year is not a prime number
            DateTime expiryDate = new DateTime(2020, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Invalid", result.Value.Result);
            Assert.Equal("MasterCard", result.Value.CardType);
        }

        /// <summary>
        /// Invalid Amex card test.
        /// </summary>
        [Fact]
        public async Task InvalidAmexCard()
        {
            // amex card number starts from 3 and has length 15
            // for this test case length will be 16
            string cardNumber = "3004567890123456";
            // expiration date
            DateTime expiryDate = new DateTime(2021, 04, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.NotEqual("Amex", result.Value.CardType);
        }
        /// <summary>
        /// Invalid JCB card test.
        /// </summary>
        [Fact]
        public async Task InvalidJCBCard()
        {
            // jcb card number starts from 3 and has length 16
            // for this test case length will be 15
            string cardNumber = "300456789012345";
            // expiration date
            DateTime expiryDate = new DateTime(2018, 12, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.NotEqual("JCB", result.Value.CardType);
        }

        /// <summary>
        /// Does not exist card test.
        /// </summary>
        [Fact]
        public async Task CardDoesNotExist()
        {
            // jcb card number starts from 3 and has length 16
            string cardNumber = "3004567890123457";
            // expiration date
            DateTime expiryDate = new DateTime(2018, 12, 01);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Does not exist", result.Value.Result);
            Assert.Equal("JCB", result.Value.CardType);
        }

        /// <summary>
        ///  Invalid data test.
        /// </summary>
        [Fact]
        public async Task InvalidData()
        {
            // random card number with length 16
            string cardNumber = "6004567890123457";
            // expiration date
            DateTime expiryDate = new DateTime(2033, 11, 11);

            // init controller
            var controller = new CardController(_mockContext.Object);
            // validate
            var result = await controller.Validate(cardNumber, expiryDate);

            // compare
            Assert.Equal("Invalid", result.Value.Result);
            Assert.Equal("Unknown", result.Value.CardType);
        }
    }
}
